import os
import sys 
import threading
from app import app
import config


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=config.PORT, debug=False, threaded=True, use_reloader=True)