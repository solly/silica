from quart import render_template, redirect, url_for, flash, request, session, abort
from app import app
from werkzeug.urls import url_parse
from flask_login import login_user, login_required, logout_user, login_manager, current_user, UserMixin
from app.models import User
from app.forms import LoginForm, NewUserForm
#from app.views.auth import add_user 
from loguru import logger

@app.route('/login', methods=['GET', 'POST'])
async def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            await flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return await render_template('login.html', title='Sign In', form=form)
    
@app.route('/logout')
def logout():
    logout_user()
    flash('You were logged out.')
    return redirect(url_for('index'))

@app.route('/add_user', methods=['GET', 'POST'])
@login_required
async def add_user():
    form = NewUserForm()
    if form.validate_on_submit():
        if not form.password1.data == form.password2.data:
            await flash('Passwords do not match.')
            return await render_template('new_user.html', title='Add new user', form=form)        
        
        return form.username.data
    return await render_template('new_user.html', title='Add new user', form=form)