# BLOCK Saltstack
from .client import SaltClient

def get_client():
    ''' returns SaltAPI connection  '''
    client = SaltClient()
    client.login()
    return client

