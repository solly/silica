import requests
import json
from loguru import logger
from dictor import dictor
from config import SALT_API, NO_TEST

class SaltClient(object):
    def __init__(self, **kwargs):
        
        username = dictor(SALT_API, 'user', checknone=True)
        password = dictor(SALT_API, 'pw', checknone=True)
        host = dictor(SALT_API, 'host', checknone=True)
        port = dictor(SALT_API, 'port', checknone=True)

        self.token = None
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.verify_ssl_cert = kwargs.get("verify_ssl_cert", False)

    def login(self):
        url = 'https://' + self.host  + ':' + str(self.port) + '/login'
        req = requests.post(url, data={'eauth': 'pam', 'username': self.username, 'password': self.password}, headers={'Accept': 'application/json'}, verify=self.verify_ssl_cert, timeout=5.0)

        if req.status_code != 200:
            raise requests.exceptions.ConnectionError("Signing in to salt (%s) failed with status code %s (body %s)" % (self.host, req.status_code, req.text))

        if req.status_code != 200:
            raise requests.exceptions.RequestException("Signing in to salt failed.")
        resp = req.json()

        self.token = dictor(resp, 'return.0.token', checknone=True)
        return self.token

    def _get_headers(self):
        return {"Accept": "application/json", "X-Auth-Token": self.token}

    def run_cmd(self, minion_id, cmd_name, args=None, run_async=False, expr_form=None):
        client = 'local_async' if run_async else 'local'
        url = 'https://' + self.host  + ':' + str(self.port)

        data = {}
        data['client'] = client
        data['tgt'] = minion_id
        data['fun'] = cmd_name

        if args:
            data['arg'] = args
        if expr_form:
            data['expr_form'] = expr_form
        
        req = requests.post(url, data=data, headers=self._get_headers(), verify=self.verify_ssl_cert)
        resp = req.json()

        data = dictor(resp, 'return.0', checknone=True)
        return data

    def run_async_cmd(self, minion_id, cmd_name, args=None):
        ''' 
        runs Salt command asyncronously 
        '''

        url = 'https://' + self.host  + ':' + str(self.port)

        if args:
            data = {"client": "local_async", "tgt": minion_id, "fun": cmd_name, "arg": args}
        else:
            data = {"client": "local_async", "tgt": minion_id, "fun": cmd_name}

        req = requests.post(url, data=data, headers=self._get_headers(), verify=self.verify_ssl_cert)
        resp = req.json()
        data = dictor(resp, 'return.0.jid', checknone=True)

        return json.dumps(data)

    def check_job_status(self, job_id):
        resp = requests.post(self.host, data={"client": "runner", "fun": "jobs.lookup_jid", "jid": job_id}, headers=self._get_headers(), verify=self.verify_ssl_cert)
        data = resp.json()
        return None

    def get_minions(self):
        url = 'https://' + self.host  + ':' + str(self.port)
        req = requests.get(url+'/minions', headers=self._get_headers(), verify=self.verify_ssl_cert)
